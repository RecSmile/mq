package com.recsmile.activemq;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * Created by Administrator on 2016/9/1.
 */
public class Sender {
    public static void main(String[] args){
        ConnectionFactory connectionFactory = null;//JMS连接工厂
        Connection connection = null;//JMS连接
        Session session = null;
        Destination destination = null;
        MessageProducer producer = null;
        connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnection.DEFAULT_USER,
                ActiveMQConnection.DEFAULT_PASSWORD,
                "tcp://localhost:61616"
        );
        try{
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);

            destination = session.createQueue("FirstQueue");
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            sendMessage(session, producer);
            session.commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }


    }

    private static void sendMessage(Session session, MessageProducer producer) throws Exception{
        for (int i = 0 ; i < 5; i++){
            producer.send(session.createTextMessage("ActiveMQ message :"+  i ));
            System.out.println("发送消息：" + "ActiveMq 发送的消息" + i);
        }
    }
}
