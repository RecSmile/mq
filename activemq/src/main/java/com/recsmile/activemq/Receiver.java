package com.recsmile.activemq;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2016/9/1.
 */
public class Receiver {
    public static void main(String[] args){
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                ActiveMQConnection.DEFAULT_USER,
                ActiveMQConnection.DEFAULT_PASSWORD,
                "tcp://localhost:61616"
        ) ;
        Connection connection =  null;
        try{
            connection = connectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination destination = session.createQueue("FirstQueue");
            MessageConsumer consumer = session.createConsumer(destination);
            while (true){
                TextMessage textMessage = (TextMessage) consumer.receive(TimeUnit.SECONDS.toMillis(100));
                if(null != textMessage){
                    System.out.println("收到消息" + textMessage.getText());
                }else {
                    break;
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(connection != null){
                try {
                    connection.close();
                } catch (JMSException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
