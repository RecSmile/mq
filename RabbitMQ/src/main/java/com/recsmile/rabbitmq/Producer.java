package com.recsmile.rabbitmq;

import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;

/**
 * Created by Administrator on 2016/9/2.
 */
public class Producer extends EndPoint{
    public Producer(String endPointName) throws IOException, TimeoutException {
        super(endPointName);
    }

    public void sendMessage(Serializable object) throws IOException{
        channel.basicPublish("", endPointName, null , SerializationUtils.serialize(object));

    }
}
