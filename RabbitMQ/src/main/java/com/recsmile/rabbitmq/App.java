package com.recsmile.rabbitmq;

import java.util.HashMap;

/**
 * Created by Administrator on 2016/9/2.
 */
public class App {

    public App() throws Exception{

        new Thread(new QueueConsumer("queue")).start();

        Producer producer = new Producer("queue");
        for (int i = 0 ; i < 1000; i++){
            HashMap message = new HashMap();
            message.put("message number", i);
            producer.sendMessage(message);
            System.out.println("Message Number "+ i +" sent.");

            Thread.sleep(500);
        }
    }

    public static void main(String[] args){
        try {
            new App();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
